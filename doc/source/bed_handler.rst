bed\_handler package
====================

Submodules
----------

bed\_handler.bed\_resize module
-------------------------------

.. automodule:: src.bed_handler.bed_resize
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.config module
--------------------------

.. automodule:: src.bed_handler.config
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.filter\_bed module
-------------------------------

.. automodule:: src.bed_handler.filter_bed
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.filter\_gene module
--------------------------------

.. automodule:: src.bed_handler.filter_gene
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.get\_gene\_locations module
----------------------------------------

.. automodule:: src.bed_handler.get_gene_locations
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.get\_gene\_regulated\_by\_ddx module
-------------------------------------------------

.. automodule:: src.bed_handler.get_gene_regulated_by_ddx
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.get\_last\_exons module
------------------------------------

.. automodule:: src.bed_handler.get_last_exons
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.get\_other\_exon\_in\_same\_gene module
----------------------------------------------------

.. automodule:: src.bed_handler.get_other_exon_in_same_gene
   :members:
   :undoc-members:
   :show-inheritance:

bed\_handler.select\_regulated\_near\_ctcf\_exons module
--------------------------------------------------------

.. automodule:: src.bed_handler.select_regulated_near_ctcf_exons
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.bed_handler
   :members:
   :undoc-members:
   :show-inheritance:
