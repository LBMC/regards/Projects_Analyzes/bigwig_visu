visu package
============

Submodules
----------

visu.config module
------------------

.. automodule:: src.visu.config
   :members:
   :undoc-members:
   :show-inheritance:

visu.figure\_maker module
-------------------------

.. automodule:: src.visu.figure_maker
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.visu
   :members:
   :undoc-members:
   :show-inheritance:
