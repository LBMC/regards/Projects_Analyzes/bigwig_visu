#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Launch the creation of GC barplots
"""

from .gc_content import create_barplot
from typing import List
from pathlib import Path
import lazyparser as lp


@lp.parse(beds="file", genome="file", environment="environment >= 0")
def launcher(beds: List[str], bed_names: List[str],
             genome: str, environment: int = 0,
             ft_name: str = "interval") -> None:
    """

    :param beds: A list of bed files containing the regions to visualise
    :param bed_names: A list of names identifying regions insides the given \
    beds.
    :param genome: A Fasta file containing a genome.
    :param environment: Number of nucleotides around the genomic intervals \
    in the beds files for which we want to know the gc content (default 0)
    :param ft_name: A name corresponding to the feature of interest \
    (default 'interval')
    """
    if len(bed_names) != len(beds):
        raise IndexError("--bed_names and --beds should contains the same "
                         "number of items")
    beds = [Path(b) for b in beds]
    create_barplot(beds, bed_names, genome, environment, ft_name)


launcher()
