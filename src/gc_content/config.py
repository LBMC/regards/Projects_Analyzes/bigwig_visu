#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: contains variables used in this modules
"""

from ..bed_handler.config import TestConfig
from pathlib import Path


class ConfigGC:
    """
    contains the folder where the gc barplot will be created
    """
    output = Path(__file__).parents[2] / "results" / "gc_barplots"
