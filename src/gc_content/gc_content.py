#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a barplot indicating \
the gc content of list of exons
"""

from .config import TestConfig, ConfigGC
from ..visu.figure_maker import load_beds
from typing import List, Any, Dict
from pyfaidx import Fasta
from Bio.Seq import Seq
from doctest import testmod
import pandas as pd
import numpy as np
from pathlib import Path
import seaborn as sns
from .gc_stats import make_stat
from .stat_annot import add_stat_annotation
import matplotlib.font_manager


def get_gc_content(bed_line: List[Any], dic_seq: Fasta) -> float:
    """
    Get the gc content of a genomic interval.

    :param bed_line: A genomic interval in a bed format
    :param dic_seq: A dictionary containing chromosomal sequences
    :return: The gc content

    >>> dic_seq = Fasta(str(TestConfig.test_fasta))
    >>> get_gc_content(["chr1", 0, 10, "s1", ".", "+"],
    ... dic_seq)
    100.0
    >>> get_gc_content(["chr1", 10, 20, "s1", ".", "+"],
    ... dic_seq)
    50.0
    >>> get_gc_content(["chr1", 20, 30, "s1", ".", "+"],
    ... dic_seq)
    0.0
    >>> get_gc_content(["chr1", 0, 10, "s1", ".", "-"],
    ... dic_seq)
    100.0
    >>> get_gc_content(["chr1", 10, 20, "s1", ".", "-"],
    ... dic_seq)
    50.0
    >>> get_gc_content(["chr1", 20, 30, "s1", ".", "-"],
    ... dic_seq)
    0.0
    >>> dic_seq.close()
    """
    seq = str(dic_seq[bed_line[0]][bed_line[1]:bed_line[2]]).upper()
    if "N" in seq:
        return np.nan
    if bed_line[5] == "-":
        seq = str(Seq(seq).reverse_complement())
    return (seq.count("G") + seq.count("C")) / len(seq) * 100


def get_many_gc_content(bed_line: List[Any], dic_seq: Fasta,
                        environment: int) -> Dict:
    """
    Get the GC content of a given genomic interval and the GC content \
    around this interval.

    :param bed_line: A genomic interval in a bed format
    :param dic_seq: A dictionary containing chromosomal sequences
    :param environment: The number of nucleotides around the interval for \
    which we want to get the gc content
    :return: A dictionary containing the GC content inside the interval \
    we want to get and around it.

    >>> dic_seq = Fasta(str(TestConfig.test_fasta))
    >>> get_many_gc_content(["chr1", 10, 20, "s1", ".", "+"],
    ... dic_seq, 0)
    {'interval': 50.0}
    >>> get_many_gc_content(["chr1", 10, 20, "s1", ".", "+"],
    ... dic_seq, 10)
    {'before': 100.0, 'interval': 50.0, 'after': 0.0}
    >>> get_many_gc_content(["chr1", 10, 20, "s1", ".", "-"],
    ... dic_seq, 10)
    {'before': 0.0, 'interval': 50.0, 'after': 100.0}
    >>> dic_seq.close()
    """
    gc_interval = get_gc_content(bed_line, dic_seq)
    if environment == 0:
        return {"interval": gc_interval}
    gc_interval = get_gc_content(bed_line, dic_seq)
    before_val = max(0, bed_line[1] - environment)
    before_interval = get_gc_content([bed_line[0], before_val,
                                      bed_line[1]] + bed_line[3:], dic_seq)
    after_val = min(len(dic_seq[bed_line[0]]), bed_line[2] + environment)
    after_interval = get_gc_content([bed_line[0], bed_line[2],
                                     after_val] +
                                    bed_line[3:], dic_seq)
    if bed_line[5] == "-":
        tmp = before_interval
        before_interval = after_interval
        after_interval = tmp
    return {"before": before_interval, "interval": gc_interval,
            "after": after_interval}


def build_gc_dataframe(bed_content: List[List], dic_seq: Fasta,
                       environment: int, ft_name: str = "interval"
                       ) -> pd.DataFrame:
    """
    Create a dataframe containing the GC content of many bed files and \
    their environment.

    :param bed_content: A list of genomic intervals of interest
    :param dic_seq: A dictionary containing genomic sequence
    :param environment: Number of nucleotide around the genomic interval for \
    which we want to know the gc content
    :param ft_name: A name corresponding to the feature of interest
    :return: a table containing the gc content of the intervals
    """
    dic = {"gc_content": [], "region": [], "location": [], "id": []}
    for bed_line in bed_content:
        res = get_many_gc_content(bed_line, dic_seq,
                                  environment)
        for k in res:
            dic["gc_content"].append(res[k])
            dic["location"].append(k.replace("interval", ft_name))
            dic["region"].append(bed_line[6])
            dic["id"].append(bed_line[3])
    return pd.DataFrame(dic)


def make_gc_barplot(df: pd.DataFrame, outfile: Path, environment: int,
                    region_names: List[str], ft_name: str) -> None:
    """
    Create a barplot showing the gc content in the given lists of intervals.

    :param df: A dataframe containing the gc content of genomics intervals
    :param outfile: The output file of interest
    :param environment: Number of nucleotide around the genomic interval for \
    which we want to know the gc content
    :param region_names: The name of the regions analysed
    :param ft_name: The name of the feature analyzed
    """
    font_files = matplotlib.font_manager.findSystemFonts(fontpaths=None,
                                                         fontext='ttf')
    font_list = matplotlib.font_manager.createFontList(font_files)
    matplotlib.font_manager.fontManager.ttflist.extend(font_list)
    sns.set(context="poster", style="white", font="Arial", font_scale=1.4)
    rgn = ", ".join(region_names[:-1]) + " and " + region_names[-1]
    title = f"GC content of {rgn} {ft_name}"
    p_vals = make_stat(df)
    if environment != 0:
        g = sns.catplot(x="location", y="gc_content", hue="region", data=df,
                        aspect=1.77, height=12, kind="violin")
                        # palette={"readthrough_ctcf": "orange",
                        #          "readthrough": "#13791C",
                        #          "no_readthrough": "#B6B6B6"})
        add_stat_annotation(g.ax, data=df, x="location", y="gc_content",
                            hue="region",
                            loc='inside',
                            box_pair_list=p_vals,
                            linewidth=2, fontsize="xx-small",
                            liney_offset_axes_coord=0.02,
                            liney_offset2box_axes_coord=0.02,
                            line_height_axes_coord=0)
    else:
        g = sns.catplot(x="region", y="gc_content", data=df,
                        aspect=1.77, height=12, kind="violin")
        add_stat_annotation(g.ax, data=df, x="region", y="gc_content",
                            loc='inside',
                            box_pair_list=p_vals,
                            linewidth=2, fontsize="xx-small",
                            liney_offset2box_axes_coord=0.1,
                            line_height_axes_coord=0)
    g.set_ylabels("GC content")
    g.set_xlabels("")
    g.fig.subplots_adjust(top=0.9)
    g.ax.tick_params(left=True, bottom=True)
    g.fig.suptitle(title + " and their surrounding regions "
                           f"of {environment} nucleotides")
    g.savefig(outfile)


def create_barplot(beds: List[Path], bed_names: List[str],
                   genome: str, environment: int,
                   ft_name: str = "interval") -> None:
    """

    :param beds: A list of bed files containing the regions to visualise
    :param bed_names: A list of names identifying regions insides the given \
    beds.
    :param genome: A Fasta file containing a genome.
    :param environment: Number of nucleotide around the genomic interval for \
    which we want to know the gc content
    :param ft_name: A name corresponding to the feature of interest
    """
    bed_content = load_beds(beds, bed_names)
    dic_seq = Fasta(genome)
    df = build_gc_dataframe(bed_content, dic_seq, environment, ft_name)
    outfile = ConfigGC.output / \
              f"gc_content_{'-'.join(bed_names)}_env_{environment}_nt.pdf"
    df.to_csv(str(outfile).replace(".pdf", ".txt"), index=False, sep="\t")
    outfile.parent.mkdir(exist_ok=True, parents=True)
    make_gc_barplot(df, outfile, environment, bed_names, ft_name)


if __name__ == "__main__":
    testmod()
