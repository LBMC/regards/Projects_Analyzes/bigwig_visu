#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create bed of exons regulated by DDX5/17 and near CTCF
"""

from .config import TestConfig, BedConfig
from .get_gene_regulated_by_ddx import load_sipp_vs_ctcf, format_exon_bed
import pandas as pd
from doctest import testmod
from .filter_gene import filter_bed
import warnings
from .get_other_exon_in_same_gene import create_gene_bed4norm
from pathlib import Path
import lazyparser as lp
from typing import List


def filter_ctcf_distance_table(df: pd.DataFrame, reg: str, threshold: int,
                               location: str, include0: bool) -> pd.DataFrame:
    """
    Filter the dataframe to recover only regulated exons near CTCF.

    :param df: The dataframe of exon regulated by CTCF
    :param reg: The regulation by CTCF
    :param threshold: The threshold distance
    :param location: The location of interest
    :param include0: True to include exons containing a CTCF site if the \
    threshold is greater than 0.
    :return: The filtered dataframe

    >>> cdf = load_sipp_vs_ctcf(TestConfig.sipp_vs_ctcf,
    ... format_exon_bed(TestConfig.exon_bed, TestConfig.gene_bed))
    >>> rdf = filter_ctcf_distance_table(cdf, 'down', 4, 'upstream', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    0    DSC2_1    -4  siPP_DOWN  1_1
    1    DSC2_2    -3  siPP_DOWN  1_2
    2    DSC2_3    -2  siPP_DOWN  1_3
    >>> rdf = filter_ctcf_distance_table(cdf, 'down', 4, 'upstream', True)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    0    DSC2_1    -4  siPP_DOWN  1_1
    1    DSC2_2    -3  siPP_DOWN  1_2
    2    DSC2_3    -2  siPP_DOWN  1_3
    4    DSC2_5     0  siPP_DOWN  1_5
    >>> rdf = filter_ctcf_distance_table(cdf, 'down', 2, 'upstream', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    2    DSC2_3    -2  siPP_DOWN  1_3
    >>> rdf = filter_ctcf_distance_table(cdf, 'down', 2, 'downstream', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    5    DSC2_6     1  siPP_DOWN  1_6
    6    DSC2_7     2  siPP_DOWN  1_7
    >>> rdf = filter_ctcf_distance_table(cdf, 'down', 2, 'both', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    2    DSC2_3    -2  siPP_DOWN  1_3
    5    DSC2_6     1  siPP_DOWN  1_6
    6    DSC2_7     2  siPP_DOWN  1_7
    >>> rdf = filter_ctcf_distance_table(cdf, 'up', 2, 'both', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist    group   id
    3    DSC2_4    -1  siPP_UP  1_4
    >>> rdf = filter_ctcf_distance_table(cdf, 'up', 2, 'downstream', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
    Empty DataFrame
    Columns: [exon_name, dist, group, id]
    Index: []
    >>> rdf = filter_ctcf_distance_table(cdf, 'all', 2, 'both', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    2    DSC2_3    -2  siPP_DOWN  1_3
    3    DSC2_4    -1    siPP_UP  1_4
    5    DSC2_6     1  siPP_DOWN  1_6
    6    DSC2_7     2  siPP_DOWN  1_7
    >>> rdf = filter_ctcf_distance_table(cdf, 'all', 2, 'both', True)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    2    DSC2_3    -2  siPP_DOWN  1_3
    3    DSC2_4    -1    siPP_UP  1_4
    4    DSC2_5     0  siPP_DOWN  1_5
    5    DSC2_6     1  siPP_DOWN  1_6
    6    DSC2_7     2  siPP_DOWN  1_7
    >>> filter_ctcf_distance_table(cdf, 'lul', 2, 'both', False)
    Traceback (most recent call last):
    ...
    ValueError: reg parameter should be one in: ['down', 'up', 'all']
    >>> filter_ctcf_distance_table(cdf, 'up', 2, 'xd', False)
    Traceback (most recent call last):
    ...
    ValueError: location parameter should be in ['upstream', \
'downstream', 'both']
    >>> rdf = filter_ctcf_distance_table(cdf, 'down', 0, 'both', False)
    >>> rdf[['exon_name', 'dist', 'group', 'id']]
      exon_name  dist      group   id
    4    DSC2_5     0  siPP_DOWN  1_5
    """
    if reg not in ['down', 'up', 'all']:
        raise ValueError(f"reg parameter should be one in: "
                         f"['down', 'up', 'all']")
    if location not in ['upstream', 'downstream', 'both']:
        raise ValueError(f"location parameter should be in "
                         f"['upstream', 'downstream', 'both']")
    if reg != "all":
        df = df.loc[df["group"] == f"siPP_{reg.upper()}", :]
    if location == "upstream":
        df = df.loc[(df["dist"] >= threshold * -1) &
                    (df["dist"] < 0 if not include0
                     else df["dist"] <= 0), :]
    elif location == "downstream":
        df = df.loc[(df["dist"] <= threshold) &
                    (df["dist"] > 0 if not include0
                     else df["dist"] >= 0), :]
    else:
        if threshold == 0:
            df = df.loc[abs(df["dist"]) == 0, :]
        else:
            if not include0:
                df = df.loc[(abs(df["dist"]) <= threshold) &
                            (df["dist"] != 0), :]
            else:
                df = df.loc[abs(df["dist"]) <= threshold, :]
    return df


def filter_expressed(exon_list: List[str]) -> List[str]:
    """
    Filter only expressed exons.

    :param exon_list: A list of exons
    :return: The list of expressed exons
    """
    egenes = BedConfig.expressed_genes.open('r').read().splitlines()
    return [exon for exon in exon_list if exon.split("_")[0] in egenes]


def create_bed_ctcf_exon(reg: str, threshold: int,
                         location: str, include0: bool = False,
                         near_ctcf: bool = True) -> None:
    """
    Filter the dataframe of exon regulated by ddx5/17 to recover only \
    regulated exons near CTCF.

    :param reg: The regulation by DDX5/17
    :param threshold: The threshold distance
    :param location: The location of interest
    :param include0: True to include exons containing a CTCF site if the \
    threshold is greater than 0.
    :param near_ctcf: True to recover exons near CTCF False to recover \
    those far from CTCF
    """
    threshold = max(threshold, 0)
    if threshold == 0:
        location = "both"
    if not include0 and threshold == 0:
        warnings.warn("include0 must be True when threshold = 0."
                      "Setting include0 to true !")
        include0 = True
    if not near_ctcf and not include0:
        warnings.warn("include0 must be True when near_ctcf is False")
        include0 = True
    i0 = "with0" if include0 else "without0"
    df_reg = load_sipp_vs_ctcf(BedConfig.sipp_vs_ctcf,
                               format_exon_bed(BedConfig.exon_bed,
                                               BedConfig.gene_bed))
    df = filter_ctcf_distance_table(df_reg, reg, threshold, location, include0)
    if near_ctcf:
        name_near = ""
        list_exons = df['id'].to_list()
    else:
        name_near = "Far_"
        if reg != "all":
            tmp_exons = df_reg.loc[df_reg["group"] == f"siPP_{reg.upper()}",
                                   "id"].to_list()
        else:
            tmp_exons = df_reg['id'].to_list()
        bad_id = df['id'].to_list() if include0 \
            else df['id'].to_list() + df.loc[df['dist'] == 0, 'id'].to_list()
        list_exons = [e for e in tmp_exons if e not in bad_id]
    list_exons = filter_expressed(list_exons)
    list_genes = [int(exon.split('_')[0]) for exon in list_exons]
    df_exon = filter_bed(BedConfig.exon_bed, list_exons)
    df_gene = filter_bed(BedConfig.gene_bed, list_genes)
    df_exon.to_csv(BedConfig.bed.output /
                   f"{name_near}CTCF_{threshold}_{location}_ddx_"
                   f"{reg}_{i0}_exon.bed",
                   sep="\t",
                   index=False)
    df_gene.to_csv(BedConfig.bed.output /
                   f"{name_near}CTCF_{threshold}_{location}_ddx_"
                   f"{reg}_{i0}_gene.bed",
                   sep="\t",
                   index=False)
    df_gene = create_gene_bed4norm(BedConfig.gene_bed, df_exon)
    df_gene.to_csv(BedConfig.bed.output /
                   f"{name_near}CTCF_{threshold}_{location}_ddx_"
                   f"{reg}_{i0}_gene-dup.bed",
                   sep="\t",
                   index=False)


@lp.parse(exon_bed="file", location=["upstream", "downstream", "both"],
          threshold="threshold >= 0")
def get_bed_ctcf_exon(exon_bed: str, threshold: int,
                      location: str, include0: bool = False,
                      near_ctcf: bool = True,
                      name_near: str = "") -> None:
    """
    Filter a bed file containing exons to recover only regulated exons \
    near or far from CTCF.

    :param exon_bed: a bed containing exons
    :param threshold: The threshold distance
    :param location: The location of interest
    :param include0: True to include exons containing a CTCF site if the \
    threshold is greater than 0.
    :param near_ctcf: True to recover exons near CTCF False to recover \
    those far from CTCF
    :param name_near: prefix of the output file name
    """
    if name_near == "":
        name_near = Path(exon_bed).name.replace(".bed", "")
    threshold = max(threshold, 0)
    if threshold == 0:
        location = "both"
    if not include0 and threshold == 0:
        warnings.warn("include0 must be True when threshold = 0."
                      "Setting include0 to true !")
        include0 = True
    if not near_ctcf and not include0:
        warnings.warn("include0 must be True when near_ctcf is False")
        include0 = True
    i0 = "with0" if include0 else "without0"
    df_reg = load_sipp_vs_ctcf(BedConfig.all_vs_ctcf,
                               format_exon_bed(Path(exon_bed),
                                               BedConfig.gene_bed))
    df_reg = df_reg[-df_reg["gene_id"].isna()]
    df_reg["gene_id"] = df_reg["gene_id"].astype(int)
    df_reg["exon_pos"] = df_reg["exon_pos"].astype(int)
    df = filter_ctcf_distance_table(df_reg, 'all', threshold, location,
                                    include0)
    if near_ctcf:
        name_near = name_near + "_near_"
        list_exons = df['id'].to_list()
    else:
        name_near = name_near + "_far_"
        tmp_exons = df_reg['id'].to_list()
        bad_id = df['id'].to_list() if include0 \
            else df['id'].to_list() + df.loc[df['dist'] == 0, 'id'].to_list()
        list_exons = [e for e in tmp_exons if e not in bad_id]
    list_exons = filter_expressed(list_exons)
    list_genes = [int(exon.split('_')[0]) for exon in list_exons]
    df_exon = filter_bed(BedConfig.exon_bed, list_exons)
    df_gene = filter_bed(BedConfig.gene_bed, list_genes)
    df_exon.to_csv(BedConfig.bed.output /
                   f"{name_near}CTCF_{threshold}_{location}_ddx_"
                   f"{i0}_exon.bed",
                   sep="\t",
                   index=False)
    df_gene.to_csv(BedConfig.bed.output /
                   f"{name_near}CTCF_{threshold}_{location}_ddx_"
                   f"{i0}_gene.bed",
                   sep="\t",
                   index=False)
    df_gene = create_gene_bed4norm(BedConfig.gene_bed, df_exon)
    df_gene.to_csv(BedConfig.bed.output /
                   f"{name_near}CTCF_{threshold}_{location}_ddx_"
                   f"{i0}_gene-dup.bed",
                   sep="\t",
                   index=False)


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 1:
        testmod()
    else:
        get_bed_ctcf_exon()
