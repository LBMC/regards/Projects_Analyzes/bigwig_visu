#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create a bed file containing the gene locations of interest.
"""

from pathlib import Path
from typing import Dict, List
from .config import BedConfig, TestConfig
from doctest import testmod


def load_exon_bed(bed_exon: Path) -> Dict:
    """
    Load a bed file containing exons.

    :param bed_exon: A bed of exons
    :return: A dictionary linking each gene to it's exons

    >>> d = load_exon_bed(TestConfig.exon_bed)[1]
    >>> d[1]
    ['18', '28681865', '28682388', '1_1', '0', '-']
    >>> d[2]
    ['18', '28681183', '28681432', '1_2', '0', '-']
    """
    dic = {}
    with bed_exon.open('r') as inbed:
        for line in inbed:
            if not line.startswith("#"):
                cline = line.replace("\n", "").split("\t")
                gene, pos = map(int, cline[3].split("_"))
                if gene not in dic:
                    dic[gene] = {pos: cline}
                else:
                    dic[gene][pos] = cline
    return dic


def get_gene_body(gene: List[str], exons: Dict) -> List:
    """
    Get the gene body of the gene `gene`.

    :param gene: A gene
    :param exons: A dictionary of the exons inside that gene
    :return: The gene body of the gene

    >>> e = {1: ['5', '100', '110', '1_1', 'Test', '+'],
    ...      2: ['5', '130', '140', '1_2', 'Test', '+'],
    ...      3: ['5', '160', '200', '1_3', 'Test', '+']}
    >>> get_gene_body(['5', '100', '200', '1', 'Test', '+'], e)
    ['5', '130', '140', '1', 'Test', '+']
    >>> e = {1: ['5', '100', '110', '1_1', 'Test', '+'],
    ...      2: ['5', '130', '140', '1_2', 'Test', '+'],
    ...      3: ['5', '160', '170', '1_3', 'Test', '+'],
    ...      4: ['5', '190', '200', '1_3', 'Test', '+']}
    >>> get_gene_body(['5', '100', '200', '1', 'Test', '+'], e)
    ['5', '130', '170', '1', 'Test', '+']
    >>> e = {4: ['5', '100', '110', '1_4', 'Test', '-'],
    ...      3: ['5', '130', '140', '1_3', 'Test', '-'],
    ...      2: ['5', '160', '170', '1_2', 'Test', '-'],
    ...      1: ['5', '190', '200', '1_1', 'Test', '-']}
    >>> get_gene_body(['5', '100', '200', '1', 'Test', '-'], e)
    ['5', '130', '170', '1', 'Test', '-']
    """
    exon_positions = sorted(list(exons.keys()))
    if gene[5] == "+":
        gene[1] = exons[exon_positions[1]][1]
        gene[2] = exons[exon_positions[-2]][2]
    else:
        gene[1] = exons[exon_positions[-2]][1]
        gene[2] = exons[exon_positions[1]][2]
    return gene


def get_gene_tss(gene: List[str], exons: Dict) -> List:
    """
    Get the gene tss of the gene `gene`.

    :param gene: A gene
    :param exons: A dictionary of the exons inside that gene
    :return: The gene tss of the gene

    >>> e = {1: ['5', '100', '110', '1_1', 'Test', '+'],
    ...      2: ['5', '130', '140', '1_2', 'Test', '+'],
    ...      3: ['5', '160', '170', '1_3', 'Test', '+'],
    ...      4: ['5', '190', '200', '1_3', 'Test', '+']}
    >>> get_gene_tss(['5', '100', '200', '1', 'Test', '+'], e)
    ['5', '100', '130', '1', 'Test', '+']
    >>> e = {4: ['5', '100', '110', '1_4', 'Test', '-'],
    ...      3: ['5', '130', '140', '1_3', 'Test', '-'],
    ...      2: ['5', '160', '170', '1_2', 'Test', '-'],
    ...      1: ['5', '190', '200', '1_1', 'Test', '-']}
    >>> get_gene_tss(['5', '100', '200', '1', 'Test', '-'], e)
    ['5', '170', '200', '1', 'Test', '-']
    """
    exon_positions = sorted(list(exons.keys()))
    if gene[5] == "+":
        gene[2] = exons[exon_positions[1]][1]
    else:
        gene[1] = exons[exon_positions[1]][2]
    return gene


def get_gene_tts(gene: List[str], exons: Dict) -> List:
    """
    Get the gene tts of the gene `gene`.

    :param gene: A gene
    :param exons: A dictionary of the exons inside that gene
    :return: The gene tts of the gene

    >>> e = {1: ['5', '100', '110', '1_1', 'Test', '+'],
    ...      2: ['5', '130', '140', '1_2', 'Test', '+'],
    ...      3: ['5', '160', '170', '1_3', 'Test', '+'],
    ...      4: ['5', '190', '200', '1_3', 'Test', '+']}
    >>> get_gene_tts(['5', '100', '200', '1', 'Test', '+'], e)
    ['5', '170', '200', '1', 'Test', '+']
    >>> e = {4: ['5', '100', '110', '1_4', 'Test', '-'],
    ...      3: ['5', '130', '140', '1_3', 'Test', '-'],
    ...      2: ['5', '160', '170', '1_2', 'Test', '-'],
    ...      1: ['5', '190', '200', '1_1', 'Test', '-']}
    >>> get_gene_tts(['5', '100', '200', '1', 'Test', '-'], e)
    ['5', '100', '130', '1', 'Test', '-']
    """
    exon_positions = sorted(list(exons.keys()))
    if gene[5] == "+":
        gene[1] = exons[exon_positions[-2]][2]
    else:
        gene[2] = exons[exon_positions[-2]][1]
    return gene


def get_after_gene(gene: List[str], size: int) -> List:
    """
    Get the gene tts of the gene `gene`.

    :param gene: A gene
    :param size: The size of the region after the gene to check

    >>> get_after_gene(['5', '100', '200', '1', 'Test', '+'], 100)
    ['5', '200', '300', '1', 'Test', '+']
    >>> get_after_gene(['5', '100', '200', '1', 'Test', '-'], 100)
    ['5', '0', '100', '1', 'Test', '-']
    """
    if gene[5] == "+":
        gene[1] = gene[2]
        gene[2] = str(int(gene[2]) + size)
    else:
        gene[2] = gene[1]
        gene[1] = str(int(gene[1]) - size)
    return gene


def write_bed(bed_file: Path, dic_exon: Dict, region: str,
              outfile: Path) -> None:
    """
    Write a bed file containing the gene body, the gene tss or tts or \
    the region after the gene.

    :param bed_file: A bed file containing the genes of interest.
    :param dic_exon: A dictionary containing the exons within the genes
    :param region: The region of interest
    :param outfile: The output file of interest
    """
    with bed_file.open('r') as infile, outfile.open('w') as out:
        for line in infile:
            if line.startswith("#"):
                out.write(line)
            else:
                cline = line.replace('\n', '').split('\t')
                gene = int(cline[3])
                exons = dic_exon[gene]
                if len(exons) < 3:
                    raise ValueError(f"The gene have to few exons "
                                     f"{len(exons)}. It should at "
                                     f"least have 3 exons")
                if region == "body":
                    cline = get_gene_body(cline, exons)
                elif region == 'tss':
                    cline = get_gene_tss(cline, exons)
                elif region == 'tts':
                    cline = get_gene_tts(cline, exons)
                else:
                    cline = get_after_gene(cline, BedConfig.size)
                out.write('\t'.join(cline) + '\n')


def create_region_bed() -> None:
    """
    Create the bed with the wanted regions.
    """
    dic_exon = load_exon_bed(BedConfig.exon_bed)
    write_bed(BedConfig.bed.filtered_gene, dic_exon, 'body',
              BedConfig.bed.body_gene)
    write_bed(BedConfig.bed.filtered_gene, dic_exon, 'tss',
              BedConfig.bed.tss_gene)
    write_bed(BedConfig.bed.filtered_gene, dic_exon, 'tts',
              BedConfig.bed.tts_gene)
    write_bed(BedConfig.bed.filtered_gene, dic_exon, 'after',
              BedConfig.bed.after_gene)


if __name__ == "__main__":
    testmod()
