#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create the bed file that will be used for the bigwig \
visualisation
"""

from .filter_gene import create_filtered_bed
from .get_gene_locations import create_region_bed
from .get_gene_regulated_by_ddx import write_regulated_gene_file
from .select_regulated_near_ctcf_exons import create_bed_ctcf_exon


def launcher():
    """
    Create the necessary bed file to visualise bigwig file
    """
    write_regulated_gene_file()
    create_filtered_bed()
    create_region_bed()
    create_bed_ctcf_exon("down", 0, "both", True)
    create_bed_ctcf_exon("down", 1000, "both", False)
    create_bed_ctcf_exon("down", 2000, "both", False)
    create_bed_ctcf_exon("down", 2000, "both", True)
    create_bed_ctcf_exon("down", 2000, "both", True, False)
    create_bed_ctcf_exon("down", 1000, "upstream", False)
    create_bed_ctcf_exon("down", 2000, "upstream", False)
    create_bed_ctcf_exon("down", 1000, "downstream", False)
    create_bed_ctcf_exon("down", 2000, "downstream", False)


launcher()
