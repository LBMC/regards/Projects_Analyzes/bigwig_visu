#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: A class containing all the variables used in this submodule
"""

from pathlib import Path


class OutputBed:
    """
    A class containing the location of output bed files
    """
    output = Path(__file__).parents[2] / "results" / "bed_file"
    filtered_gene = output / "filtered_gene.bed"
    body_gene = output / "body_gene.bed"
    tss_gene = output / "tss_gene.bed"
    tts_gene = output / "tts_gene.bed"
    after_gene = output / "after_gene.bed"
    ddx_genes = output / "DDX5_17_genes.txt"


class BedConfig:
    """
    A class containing all the variables used in this submodule
    """
    base = Path(__file__).parents[2]
    gene_bed = base / "data" / "bed" / "gene.bed"
    exon_bed = base / "data" / "bed" / "exon.bed"
    sipp_vs_ctcf = base / "data" / "Sipp_exon_vs_CTCF.csv"
    all_vs_ctcf = base / 'data' / 'All_Exons_vs_CTCF.csv'
    exons_vs_ctcf = base / "data" / "All_Exons_vs_CTCF.csv"
    expressed_genes = base / "data" / "5y_expressed_genes_basemean>5.txt"
    bed = OutputBed
    size = 5000


class TestConfig:
    """Contains variable used in docstring tests"""
    base = Path(__file__).parents[2] / "tests" / "files"
    list_genes = base / "list_genes.txt"
    gene_bed = base / "genes.bed"
    exon_bed = base / "exons.bed"
    small_bw = base / "small.bw"
    sipp_vs_ctcf = base / 'Sipp_exon_vs_CTCF.csv'
    test_fasta = base / "test_genome.fa"
