#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Contains functions to filter the gene of interest in a bed file.
"""

import pandas as pd
from pathlib import Path
from doctest import testmod
from .config import BedConfig, TestConfig
from typing import List


def select_gene_of_interest(gene_file: Path) -> List[int]:
    """
    Get the fasterDb gene id located in tge file `gene_file`.

    :param gene_file: A file containing a list of gene of interest
    :return: The list of gene of interest

    >>> select_gene_of_interest(TestConfig.list_genes)
    [73, 75, 89, 123, 128]
    """
    with gene_file.open('r') as infile:
        gene_list = infile.read().splitlines()
    return [int(gene_id) for gene_id in gene_list if gene_list]


def filter_bed(bed_file: Path, gene_list: List[int]) -> pd.DataFrame:
    """
    load a bed containing FasterDB gene and only recover the gene of \
    interest within it.

    :param bed_file: A bed file containing genes
    :param gene_list: a list of gene of interest
    :return: The bed file bed containing only genes located in gene_list

    >>> filter_bed(TestConfig.gene_bed, [1, 5, 9])
       #ref     start       end  id     score strand
    0    18  28645943  28682388   1      DSC2      -
    4    13  45766989  45775176   5     KCTD4      -
    8    13  45967450  45992516   9  SLC25A30      -
    """
    df = pd.read_csv(bed_file, sep="\t")
    return df[df["id"].isin(gene_list)]


def create_filtered_bed() -> None:
    """
    Create a bed file containing only the genes of interest.
    """
    gene_list = select_gene_of_interest(BedConfig.bed.ddx_genes)
    df = filter_bed(BedConfig.gene_bed, gene_list)
    df.to_csv(BedConfig.bed.filtered_gene, sep="\t", index=False)


if __name__ == "__main__":
    testmod()
