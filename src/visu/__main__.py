#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:  Create a figure showing the ChIP-Seq coverage of particular \
gene regions from ChIP-seq experiment.
"""

from .figure_maker import create_figure
import lazyparser as lp
from pathlib import Path
from typing import List, Optional


@lp.parse(design='file', region_beds='file',
          nb_bin="nb_bin > 5", figure_type=['metagene', 'barplot'],
          show_replicate=['y', 'n', 'Y', 'N'])
def launcher(design: str, bw_folder: str, region_beds: List[str],
             region_names: List[str], nb_bin: int = 100,
             figure_type: str = 'metagene', norm: str = 'None',
             show_replicate: str = 'y', environment: List[int] = (0, 0),
             border_names: List[str] = ('', ''),
             output: str = '.', ylim: List[float] = (None, None),
             stat: bool = False) -> None:
    """
    Create A metagene or a barplot figure from bigwig file on regions defined \
    in the bed file provided with 'region_bed' parameter.

    :param design: A tabulated file containing 3 columns. The first columns \
    contains a bigwig filename, the second contains the condition name and \
    the last one contains the replicate of the condition.
    :param bw_folder: The folder containing the bigwig file mentioned in \
    the first column of the 'design' table.
    :param region_beds: A list of bed files containing the regions to visualise
    :param region_names: A list of names identifying regions insides \
    the given beds.
    :param nb_bin: The number of bins used to represents the regions of \
    'region_bed'.
    :param figure_type: The kind of representation wanted (barplot or metagene)
    :param norm: A number corresponding to a bin, a file with \
    the normalisation value to apply for each replicate. 'None' for no \
    normalisation (default 'None')
    :param show_replicate: 'y' to create a figure showing the replicate \
    'n' else.
    :param environment: A list of two int. The first contains the number of \
    nucleotide to represent around the region of interest and the second,
    the number of bin used to represent those surrounding regions.
    :param border_names: The name of the borders
    :param output: Folder where the results will be created
    :param ylim: The y-axis range (default None)
    :param stat: A boolean indicating whether to perform \
    statistical analysis. This parameter can only be set to True \
     if figure_type is 'metagene' (default: False)
    """
    if ylim[0] is not None and (len(ylim) != 2 or ylim[0] >= ylim[1]):
        raise ValueError("The ylim must have two values and the first value "
                         "must be lower to the second value")
    if environment[0] < 0 or environment[1] < 0 or \
            environment[0] < environment[1]:
        raise ValueError(f"The two values given with --environment must "
                         f"be greater than 0 and the first value must be "
                         f"greater than the second")
    show_rep = show_replicate.lower() == 'y'
    norm = int(norm) if norm.isdigit() else None if norm == 'None' else norm
    if stat and figure_type != "metagene":
        raise NotImplementedError(f"the stat parameter can only be used if "
                                  f"figure type is metagene")
    if isinstance(norm, str):
        norm = Path(norm)
        if not norm.is_file():
            raise FileNotFoundError(f"The file {norm} was not found")
    reg_beds = [Path(p) for p in region_beds]
    create_figure(Path(design), Path(bw_folder), reg_beds,
                  region_names, nb_bin, figure_type, norm, show_rep,
                  environment, border_names, Path(output), ylim, stat)


launcher()
